/**
 * 文章页面
 * @author KAAAsS
 */
/*jshint unused: false */
/*jshint evil: true */
/*jshint -W106 */
(function() {
    /*
     * 添加各类引用
     */
    if (!_hasHelper) {
        // 加入echarts
        $.getScript("http://cdn.bootcss.com/echarts/3.1.10/echarts.min.js");
        // 加入FileSaver
        $.getScript("http://cdn.bootcss.com/FileSaver.js/2014-11-29/FileSaver.min.js");
        // 外部样式表
        $("head").append("<link>").children("link:last").attr({
            rel: "stylesheet",
            type: "text/css",
            href: "http://work.bcpu.tk/im9helper/css/main.css"
        });
        _hasHelper = true;
    }

    var _aValue = ["发帖数量统计", "发帖时间分布", "下载表格"],
        $box = $("<div class='chartbox'></div>").insertBefore(".table-nav"), // 图表盒
        $chartlist = $('<div class ="chart"></div>'.x(_aValue.length - 1)).css("display", "none").appendTo($box); // 图表列表
    $('<div class="progress"><div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 0%">0%</div></div>').css("display", "none").appendTo($box); // 进度条
    var posts = [], // 帖子信息
        $chartinfo = $('<div class = "chartinfo">正在载入，请稍后……</div>').appendTo($box), // 信息显示区
        $buttlist = $('<input type = "button" class = "chartbutton"/>'.x(_aValue.length)).each(function(i) {
            $(this).val(_aValue[i]);
        }), // 按钮列表
        tips = []; // 提示列表

    /*
    开始收集数据
     */

    var $loadimg = $("<img></img>").attr("src", "http://static.yo9.com/web/static/loading.gif?e11a9bf").css("margin", "50px 350px").prependTo($box),
        _progress = 0, // 加载进度
        _totalPage = 99, // 总页数
        pages = 0;

    /**
     * 错误处理
     * @param  {string} str 错误信息
     */
    function _err(str) {
        showFloatBox({ // 显示提示框
            content: str
        });
        _hasHelper = false; // 取消脚本运行状态
        $box.remove(); // 恢复DOM
    }

    /**
     * 请求获得数据
     * @param  {int} 获取页码，递归用
     */
    var time = new Date().getTime() * 1000;
    $('.progress').css("display", "block"); // 显示进度条
    (function query(pageNo) {
        var param = {
            page_no: pageNo,
            // page_size: 100,
            community_id: community_id,
            captcha: window.captcha_key,
            ts: time
        };
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open('GET', apiManageList['QueryPostList'] + '?' + $.param(param));
        xhr.onload = function() { // 成功取得处理数据
            var data = $.parseJSON(xhr.responseText);
            if (xhr.status !== 200 || data.code !== 0) { // 失败则_totalPage置零，停止请求报错
                xhr.abort();
                _totalPage = 0;
                _err('帖子数据加载失败，' + data.message);
                return;
            }
            _totalPage = data['data']['total_page']; // 获取总页数
            Array.prototype.push.apply(posts, data.data.result); // 合并数据进数组
            if (pageNo === 1) { // 如果为刚开始，创建请求队列
                for (var i = 2; i <= _totalPage; i++)
                    query(i);
            }
            pages++; // Page计数
            _progress = pages / _totalPage * 100; // 计算进度
            $('.progress').children().attr("aria-valuenow", _progress)
                .css("width", _progress + '%').text(_progress.toFixed(2) + '%'); // 更改进度条
            $chartinfo.text("已加载" + pages + "页，共" + _totalPage + "页……"); // 更新提示
            if (pages === _totalPage) { // 结束递归条件
                $('.progress').css("display", "none"); // 隐藏进度条
                $loadimg.css("display", "none"); // 隐藏加载动画
                $chartinfo.text("总帖子数：" + data['data']['total_count'] + ' 总页数：' + _totalPage);
                quickSort(posts, 'publish_time');
                onQueryEnd();
            }
        };
        xhr.send();
    })(1);

    /**
     * 当请求结束后的回调
     */
    function onQueryEnd() {
        // console.debug(posts);
        $buttlist.each(function(index, el) {
            if (index < $buttlist.length - 1) { // 除去表格按钮
                $box.append(el);
                el.onclick = function() {
                    $chartlist.css('display', 'none');
                    $box.scrollTo();
                    $($chartlist[index]).css("display", "block");
                    if ($($chartlist[index]).children().length) {
                        $chartinfo.text(tips[index]);
                        return;
                    }
                    eval('createChart' + (index + 1))();
                };
            }
        });
        $box.append($buttlist[$buttlist.length - 1]);
        $buttlist[$buttlist.length - 1].onclick = function() {
            downloadform();
        };
    }

    /**
     * 生成发帖数量统计图表
     * @author origin: airhiki, modified: KAAAsS
     */
    function createChart1() {
        function dataHandle(list) { //数据处理
            var data = [],
                datares = [
                    [],
                    []
                ],
                timecount = new Date(list[0]['publish_time'].slice(0, 10) + " 00:05:00"), //起始时间
                timeend = new Date(),
                timebase;

            while (timecount < timeend) { //填充时间轴
                data[timecount.toLocaleDateString()] = 0;
                timecount.setTime(timecount.getTime() + 86400000); //续一天
            }
            for (var i in list) { //填数据
                timebase = new Date(list[i]['publish_time']);
                data[timebase.toLocaleDateString()]++;
            }
            for (var e in data) { //拆分坐标轴
                datares[0].push(e);
                datares[1].push(data[e]);
            }
            return datares;
        }
        $chartinfo.text(tips[0] = '管理的好的话，会发帖也会更积极呢~(提示:统计去除删除的帖子)');
        var myChart = echarts.init($chartlist[0]),
            datares = dataHandle(posts);
        myChart.showLoading(); // 加载动画
        var option = { // 指定图表的配置项和数据
            title: {
                text: '圈日发帖量',
                subtext: tableInfo
            },
            backgroundColor: '#fff',
            dataZoom: [{ // 这个dataZoom组件，默认控制x轴。
                type: 'slider', // 这个 dataZoom 组件是 slider 型 dataZoom 组件
                start: 0, // 左边在 10% 的位置。
                end: 100 // 右边在 60% 的位置。
            }, {
                type: 'inside',
                start: 0,
                end: 100
            }],
            grid: {
                left: '3%',
                right: '4%',
                bottom: '8%',
                containLabel: true
            },
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            legend: {
                data: ['日发帖量']
            },
            xAxis: {
                data: datares[0],
                boundaryGap: false
            },
            yAxis: {},
            series: [{
                name: '日发帖量',
                type: 'line',
                data: datares[1],
                markLine: {
                    data: [{
                        type: 'average',
                        name: '平均值'
                    }]
                }
            }]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.hideLoading();
        myChart.setOption(option);
    }

    /**
     * 生成发帖时间分布图表
     */
    function createChart2() {
        _err('比较懒还没做');
    }

    /**
     * 下载数据表格
     */
    function downloadform() {
        var table = $("body").append("<table>").children("table:last")
            .addClass("usertable").css("display", "none")
            .append("<h1>帖子列表(" + tableInfo + ")</h1>")
            .append("<tr><th>UID</th><th>用户名</th><th>帖子标题</th><th>发帖时间</th><th>POST_ID</th></tr>");
        for (var i = 0; i < posts.length; i++) {
            $("<tr><td>" + posts[i]['member_id'] + "</td><td>" + posts[i].username + "</td><td>" + posts[i].title + "</td><td>" + posts[i]['publish_time'] + "</td><td>" + posts[i]['post_id'] + "</td></tr>").appendTo(table);
        }
        var blob = new Blob([table[0].outerHTML], {
            type: "text/plain;charset=utf-8"
        });
        saveAs(blob, "帖子列表(" + tableInfo + ").xls");
    }
})();
