/**
 * 团队管理页面
 * @author KAAAsS
 */
/*jshint -W106 */
// 一些公用方法
Date.prototype.format = function(format) {
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(), //day
        "h+": this.getHours(), //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
};
// 逻辑开始
$('<div class="groupButton set" id="magician">钦点一番</div>').appendTo('.level-3>.group-team-top');
$('#magician').click(
    function() {
        var phoneCode = '<div class="phone groupButton">获得验证码</div>';
        showFloatBox({
            id: 'addAdmin',
            content: '<div>' +
                '<div class="message_box_block">' +
                '<div class="condition-block-input"><em>成员ID：</em><input style="width: 200px" id="uid" type="text"></div>' +
                '<div class="condition-block-input"><em>手机验证码：</em><input style="width: 80px" id="sms-code" type="text">' + phoneCode + '</div></div>' +
                '<div class="box-button-list">' +
                '</div>',
            cancel: true,
            title: "直接任命",
            width: '500',
            sure: '确定',
            callback: function() {
                var uid = $('#uid').val();
                if (!uid) {
                    showFloatBox({
                        content: '请填写用户ID'
                    });
                    return;
                }
                var smsCode = $('#sms-code').val();
                if (!uid || !smsCode) {
                    showFloatBox({
                        content: '请填写用户uid及验证码'
                    });
                    return;
                } else {
                    ajaxQueue.ajax({
                        url: apiManageList['AddCommunityAdministrator'],
                        type: "post",
                        dataType: "json",
                        data: {
                            'community_id': community_id,
                            'user_name': uid,
                            'sms_code': smsCode,
                            'role_id': 3
                        },
                        success: function(result) {
                            if (result.code === 0) {
                                showFloatBox({
                                    content: '任命成功',
                                    callback: function() {
                                        $('.floatMainBox').fadeOut(function() {
                                            $('#addAdmin').remove();
                                            $('.floatBox').remove();
                                        });
                                    }
                                });
                                getCommunityAdminList();
                            } else {
                                getErrorMessage(result.code, true);
                                return;
                            }
                        },
                        error: function() {
                            getErrorMessage(result.code, true);
                        }
                    });
                }
            }

        });
        $('.phone.groupButton').click(function() {
            //获取手机验证码
            ajaxQueue.ajax({
                url: apiCommonList['ApplySmsCode'],
                type: "get",
                dataType: "json",
                data: {
                    'data_type': 1
                },
                success: function(result) {
                    if (result.code === 0) {
                        showFloatBox({
                            content: '验证码已发送，请注意查收'
                        });
                    } else {
                        getErrorMessage(result.code, true);
                    }
                },
                error: function() {
                    getErrorMessage(result.code, true);
                }
            });
        });
    }
);
// 申请条件
function loadRoleModel() {
    ajaxQueue.ajax({
        url: apiCommonList['RoleModel'],
        type: "get",
        dataType: "json",
        data: getAjaxData({}),
        success: function(result) {
            $('.post-name.level-2').html(result.data[0].role_name);
            $('.level-2 .post-count').val(result.data[0].post_condition);
            $('.level-2 .reply-count').val(result.data[0].reply_condition);
            $('.team-number.level-2 span').html(result.data[0].member_limit);
            $('.post-name.level-3').html(result.data[1].role_name);
            $('.level-3 .post-count').val(result.data[1].post_condition);
            $('.level-3 .reply-count').val(result.data[1].reply_condition);
            $('.team-number.level-3 span').html(result.data[1].member_limit);
        },
        error: function() {}
    });
}
var adminConditionBlock = '<div class="admin-condition level-2"><span id="title">组长申请条件</span><div class="condition-block"><div class="condition-block-input"><span>发帖数</span><input class="post-count" type="text" disabled="true"></div><div class="condition-block-input"><span>回复数</span><input class="reply-count" type="text" disabled="true"></div><div class="condition button set" level="2">修改</div></div></div>';
$('.group-team:lt(2)').append(adminConditionBlock);
$('.group-team.level-3 .admin-condition').removeClass('level-2');
$('.group-team.level-3 .admin-condition').addClass('level-3');
$('.group-team.level-3 .admin-condition .set').attr('level', '3');
$('.admin-condition.level-3 #title').html('小组长申请条件');
$('.condition-block .button').click(function() {
    var _this = this;
    var roleLevel = $(this).attr('level');
    var postCount = $('.admin-condition.level-' + roleLevel + ' .post-count').val();
    var replyCount = $('.admin-condition.level-' + roleLevel + ' .reply-count').val();
    if ($(this).hasClass('set')) {
        $(this).html('确认');
        $(this).addClass('save');
        $(this).removeClass('set');
        $('.admin-condition.level-' + roleLevel + ' input').removeAttr("disabled");
    } else {
        if (!postCount) {
            showFloatBox({
                content: '发帖数不能为空'
            });
            return;
        }
        if (!replyCount) {
            showFloatBox({
                content: '回帖数不能为空'
            });
            return;
        }
        ajaxQueue.ajax({
            url: apiManageList['UpdateCommunityAdministratorCondition'],
            type: "post",
            dataType: "json",
            data: {
                // title : "冷鬼",
                // avatar : "",
                // reason : "前面申请的冷鬼组不见了！"
                community_id: community_id,
                role_id: roleLevel,
                post_count: postCount,
                reply_count: replyCount
            },
            success: function(result) {
                if (result.code === 0) {
                    $(_this).html('修改');
                    $(_this).addClass('set');
                    $(_this).removeClass('save');
                    $('.admin-condition.level-' + roleLevel + ' input').attr('disabled', 'true');
                    showFloatBox({
                        content: "修改成功"
                    });
                } else {
                    getErrorMessage(result.code, true);
                }
            },
            error: function(result) {
                getErrorMessage(result.code, true);
            }
        });
    }
});
loadRoleModel();
// 小组长执行状况
ajaxQueue.ajax({
    url: apiManageList['QueryCommunityMngDetailList'],
    type: "GET",
    dataType: "json",
    data: {
        community_id: community_id,
        username: '',
        time_start: '2015-01-01',
        time_end: new Date().format('yyyy-MM-dd')
    },
    success: function(result) {
        if (result.code === 0) {
            $('.auxiliary').height(185);
            for (var index in result.data.result) {
                $('.admin-info:contains(' + result.data.result[index].userName + ')').append('<p>处理举报：' + result.data.result[index].dealReportCnt + '</p>')
                    .append('<p>处理申诉：' + result.data.result[index].dealBanCnt + '</p>')
                    .append('<p>封禁用户：' + result.data.result[index].banCnt + '</p>')
                    .append('<p>删除帖子：' + result.data.result[index].delPostCnt + '</p>')
                    .append('<p>删除回复：' + result.data.result[index].delPostReplyCnt + '</p>');
            }
        } else {
            getErrorMessage(result.code, true);
            return;
        }
    },
    error: function() {
        getErrorMessage(result.code, true);
    }
});
